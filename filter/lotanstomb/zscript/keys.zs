//-------------------------------------------------
// Objectives in lieu of skull keys
//-------------------------------------------------
class HDLTObjective:HDRedKey abstract{
	override void PostBeginPlay(){
		HDUPKAlwaysGive.PostBeginPlay();
	}
	states{
	spawn2:
		#### A 1 A_SetTics(random(4,50));
		#### B 1 A_SetTics(random(4,15));
		loop;
	}
}
class HDRedObjective:HDLTObjective replaces RedSkull{
	default{
		hdupkalwaysgive.toallplayers "RedSkull";
		hdupkalwaysgive.msgtoall "$PICK_REDSKULL";
	}
	states{
	spawn:
		RSKU A 0;
		goto spawn1;
	}
}
class HDBlueObjective:HDLTObjective replaces BlueSkull{
	default{
		hdupkalwaysgive.toallplayers "BlueSkull";
		hdupkalwaysgive.msgtoall "$PICK_BLUESKULL";
	}
	states{
	spawn:
		BSKU A 0;
		goto spawn1;
	}
}
class HDYellowObjective:HDLTObjective replaces YellowSkull{
	default{
		hdupkalwaysgive.toallplayers "YellowSkull";
		hdupkalwaysgive.msgtoall "$PICK_YELLOWSKULL";
	}
	states{
	spawn:
		YSKU A 0;
		goto spawn1;
	}
}
