
//Ammo box and components
sprite AMMOA0,31,19{
	offset 16,19
	patch AMBXA0,0,0{}
}
sprite OWWVA0,31,28{
	offset 16,28
	patch AMBXB0,0,0{}
}
graphic MDBXSIGN,25,6{
	patch AMBOX,-32,0{}
}
graphic AMBXSIGN,25,7{
	patch AMBOX,-31,-6{}
}
sprite AMBXA0,31,19{
	offset 16,19
	patch AMBOX,0,-1{}
	patch AMBXSIGN,3,11{}
}
sprite AMBXB0,31,28{
	offset 16,28
	patch AMBOX,-31,-13{}
	patch AMBXSIGN,3,20{}
}
sprite AMBXC0,31,19{
	offset 16,19
	patch AMBOX,0,-20{}
	patch AMBXSIGN,3,5{rotate 180}
}


//Squad summoner
sprite PRIFA0,55,24{
	offset 24,20
	patch ARM1A0,6,2{translation "112:127=144:151","118:124=108:111"}
	patch BRLLD0,0,9{}
	patch BON2A0,0,0{translation "112:127=192:207"}
}


//Yokai
sprite YOKAA0,30,30{
	offset 15,30
	patch PINSA0,0,0{translation "red"}
}
sprite YOKAB0,30,30{
	offset 15,30
	patch PINSB0,0,0{translation "red"}
}
sprite YOKAC0,30,30{
	offset 15,30
	patch PINSC0,0,0{translation "red"}
}
sprite YOKAD0,30,30{
	offset 15,30
	patch PINSD0,0,0{translation "red"}
}


sprite BPAKB0,30,26{
	offset 14,26
	xscale 0.83
	yscale 1.2
	patch BPAKA0,0,0{rotate 270}
}
sprite BPAKC0,26,30{
	offset 13,30
	patch STASQGR,8,0{}
	patch BPAKA0,0,1{}
}


sprite ARMSB0,37,28{
	offset 0,-8
	patch ARMSA0,0,0{}
}
sprite ARMCB0,37,26{
	offset 0,-8
	patch ARMCA0,0,0{}
}


sprite GATEA0,64,64{
	offset 32,32
	patch GATE3,0,0{}
}
sprite GATEB0,64,64{
	offset 32,32
	patch GATE4,0,0{}
}


flat HDWINDOW,64,64{
	patch FLAT19,0,0{}
}


//plant bits
sprite SPLTA0,2,2{
	offset 1,2
	patch PLANTBIT,-30,-7{}
}
sprite SPLTB0,3,4{
	offset 1,4
	patch PLANTBIT,-29,-12{}
}
sprite SPLTC0,6,6{
	offset 2,6
	patch PLANTBIT,-5,-10{}
}
sprite SPLTD0,8,16{
	offset 2,16
	patch PLANTBIT,-11,-0{}
}
sprite SPLTE0,4,13{
	offset 1,13
	patch PLANTBIT,-19,-3{}
}
sprite SPLTF0,5,11{
	offset 3,11
	patch PLANTBIT,0,-5{}
}
sprite SPLTG0,7,14{
	offset 3,14
	patch PLANTBIT,-22,-2{}
}


//Boss clips
sprite RCLPA0,5,14{
	yscale 2.0
	offset 2,14
	patch CMAGA0,-7,8{}
	patch CMAGA0,-7,-1{}
	patch RBRSA3A7,1,0{}
	patch RBRSA3A7,1,1{}
	patch RBRSA3A7,1,3{}
	patch RBRSA3A7,1,4{}
	patch RBRSA3A7,1,6{}
	patch RBRSA3A7,1,7{}
	patch RBRSA3A7,1,9{}
	patch RBRSA3A7,1,10{}
	patch RBRSA3A7,1,12{}
	patch RBRSA3A7,1,13{}
}
sprite RCLPB0,5,14{
	yscale 2.0
	offset 2,14
	patch CMAGA0,-7,8{}
	patch CMAGA0,-7,-1{}
	patch RBRSA3A7,1,3{}
	patch RBRSA3A7,1,4{}
	patch RBRSA3A7,1,6{}
	patch RBRSA3A7,1,7{}
	patch RBRSA3A7,1,9{}
	patch RBRSA3A7,1,10{}
	patch RBRSA3A7,1,12{}
	patch RBRSA3A7,1,13{}
}
sprite RCLPC0,5,14{
	yscale 2.0
	offset 2,14
	patch CMAGA0,-7,8{}
	patch CMAGA0,-7,-1{}
	patch RBRSA3A7,1,6{}
	patch RBRSA3A7,1,7{}
	patch RBRSA3A7,1,9{}
	patch RBRSA3A7,1,10{}
	patch RBRSA3A7,1,12{}
	patch RBRSA3A7,1,13{}
}
sprite RCLPD0,5,14{
	yscale 2.0
	offset 2,14
	patch CMAGA0,-7,8{}
	patch CMAGA0,-7,-1{}
	patch RBRSA3A7,1,9{}
	patch RBRSA3A7,1,10{}
	patch RBRSA3A7,1,12{}
	patch RBRSA3A7,1,13{}
}
sprite RCLPE0,5,14{
	yscale 2.0
	offset 2,14
	patch CMAGA0,-7,8{}
	patch CMAGA0,-7,-1{}
	patch RBRSA3A7,1,12{}
	patch RBRSA3A7,1,13{}
}
sprite RCLPF0,14,5{
	xscale 1.6
	offset 7,2
	patch CMAGA0,8,3{rotate 270}
	patch CMAGA0,-1,3{rotate 270}
}




//Ripsaw
sprite BEVGA0,140,55{
	offset -182,-120
	xscale 1.0
	yscale 1.0
	patch SAWGA0,0,0 {}
}
sprite BEVGB0,140,55{
	offset -182,-120
	xscale 1.0
	yscale 1.0
	patch SAWGB0,0,0 {}
}
sprite BEVGC0,153,89{
	offset -182,-126
	xscale 1.1
	yscale 1.1
	patch SAWGC0,0,0 {}
}
sprite BEVGD0,154,89{
	offset -182,-126
	xscale 1.1
	yscale 1.1
	patch SAWGD0,0,0 {}
}




//second pistol
sprite PI2GA0,65,92{
	offset -119,-88
	patch PI1GA0,0,0{flipx}
}
sprite PI2GB0,65,96{
	offset -119,-87
	patch PI1GB0,0,0{flipx}
}
sprite PI2GC0,68,100{
	offset -116,-86
	patch PI1GC0,0,0{flipx}
}
sprite PI2GD0,72,106{
	offset -119,-76
	patch PI1GD0,0,0{flipx}
}
sprite PI2GE0,77,118{
	offset -117,-68
	patch PI1GE0,0,0{flipx}
}
sprite PI2FA0,32,31{
	offset -143,-73
	patch PI1FA0,0,0{flipx}
}



//SMG
sprite SMGNA0,50,19{
	offset 23,19
	patch SMGNM0,27,7{}
	patch SMGNB0,0,0{}
}
sprite SMGNB0,47,11{
	offset 24,11
	patch SMSNB0,0,-2{}
}
sprite SMSNA0,47,21{
	offset 24,21
	patch SMGNM0,25,9{}
	patch SMSNB0,0,2{}
}
sprite SMGNA0,47,19{
	offset 24,19
	patch SMGNM0,25,7{}
	patch SMGNB0,0,0{}
}
sprite SMGGA0,52,95{
	offset -134,-97
	patch SMGG1,38,21{}
	patch SMGG,0,0{}
}
sprite SMGGB0,54,95{
	offset -129,-95
	xscale 0.97
	yscale 0.97
	patch SMGG2,39,24{}
	patch SMGG,0,0{}
}
sprite SMSGA0,52,95{
	offset -134,-97
	patch SMGG1,38,21{}
	patch SMGG,0,0{}
	patch SMGS,8,0{}
}
sprite SMSGB0,54,95{
	offset -129,-95
	xscale 0.97
	yscale 0.97
	patch SMGG2,39,24{}
	patch SMGG,0,0{}
	patch SMGS,8,0{}
}



//Frag grenade throw HUD
sprite FRGGB0,80,13{
	xscale 1.6
	yscale 1.6
	offset -216,-180
	patch M_BACK_S,0,0{flipx}
	patch M_BACK_S,64,0{}
}
sprite FRGGC0,80,13{
	xscale 1.6
	yscale 1.6
	offset -216,-160
	patch M_BACK_S,0,0{flipx}
	patch M_BACK_S,64,0{}
}
sprite FRGGD0,80,59{
	xscale 1.6
	yscale 1.6
	offset -216,-125
	patch M_BACK_S,0,0{flipx}
	patch M_BACK_S,64,0{}
	patch M_BACK_S,0,35{flipx}
	patch M_BACK_S,64,35{}
}
sprite FRGGE0,80,94{
	xscale 1.6
	yscale 1.6
	offset -216,-90
	patch M_BACK_S,0,0{flipx}
	patch M_BACK_S,64,0{}
	patch M_BACK_S,0,35{flipx}
	patch M_BACK_S,64,35{}
	patch M_BACK_S,0,70{flipx}
	patch M_BACK_S,64,70{}
}


//Empty armour bonus pickup
sprite BON2E0,13,19{
	offset 6,19
	patch BON2D0,0,0{translation desaturate,31}
}


//automap
sprite PMAPA0,28,27{
	offset 13,25
	patch PMAPA0,0,0{}
}
sprite PMAPB0,28,27{
	offset 13,25
	patch PMAPB0,0,0{}
}
sprite PMAPC0,28,27{
	offset 13,25
	patch PMAPC0,0,0{}
}
sprite PMAPD0,28,27{
	offset 13,25
	patch PMAPD0,0,0{}
}
sprite PMAPE0,28,27{
	offset 13,25
	patch PMAPA0,0,0{}
	patch PMAPD0,0,0{translation "112:127=240:247";alpha 0.2;style translucent}
}
sprite PMAPF0,28,27{
	offset 13,25
	patch PMAPB0,0,0{}
	patch PMAPD0,0,0{translation "112:127=240:247";alpha 0.2;style translucent}
}
sprite PMAPG0,28,27{
	offset 13,25
	patch PMAPC0,0,0{}
	patch PMAPD0,0,0{translation "112:127=240:247";alpha 0.2;style translucent}
}
sprite PMAPH0,28,27{
	offset 13,25
	patch PMAPD0,0,0{}
	patch PMAPD0,0,0{translation "112:127=240:247";alpha 0.2;style translucent}
}



//ladder
sprite LADDA0,17,19{
	offset 8,19
	patch hdladder,0,0{}
}
sprite LADDB0,9,12{
	offset 5,12
	patch hdladder,-17,0{}
}
sprite LADDC0,17,7{
	offset 8,0
	patch hdladder,0,-12{flipy}
}
sprite LADDD0,17,19{
	offset 12,19
	patch LADDC0,0,12{}
	patch LADDB0,8,6{}
	patch LADDB0,4,0{}
	patch LADDB0,6,3{}
	patch LADDA0,0,0{}
}



//ZM66 pickup
sprite RIFSD0,42,16{
	offset 20,15
	patch ZMBASE,0,0{}
}
sprite RIGSD0,42,16{
	offset 20,15
	patch RIFSD0,0,0{}
	patch RIFGLAT,21,8{}
}
sprite RIFLD0,42,16{
	offset 20,15
	patch ZMBASE,0,0{}
	patch FIRESEL,16,8{}
}
sprite RIGLD0,42,16{
	offset 20,15
	patch RIFLD0,0,0{}
	patch RIFGLAT,21,8{}
}
sprite RIFSA0,42,17{
	offset 20,16
	patch ZMMAGAT,6,10{}
	patch RIFSD0,0,0{}
}
sprite RIFLA0,42,17{
	offset 20,16
	patch ZMMAGAT,6,10{}
	patch RIFLD0,0,0{}
}
sprite RIGLA0,42,17{
	offset 20,16
	patch ZMMAGAT,6,10{}
	patch RIGLD0,0,0{}
}
sprite RIGSA0,42,17{
	offset 20,16
	patch ZMMAGAT,6,10{}
	patch RIGSD0,0,0{}
}
//cookoff sprites
sprite RIFLB5,5,17{
	offset 3,16
	patch RIFBC,-128,-17{}
}
sprite RIFLB6B4,23,17{
	offset 10,16
	patch RIFBC,-134,-17{}
}
sprite RIFLB7B3,42,17{
	offset 20,16
	patch RIFBC,-157,-17{}
}
sprite RIFLB8B2,23,17{
	offset 10,16
	patch RIFBC,-199,-17{}
}
sprite RIFLB1,5,17{
	offset 3,16
	patch RIFBC,-223,-17{}
}
sprite RIGLB5,7,17{
	offset 4,16
	patch RIFBC,-127,0{}
}
sprite RIGLB6B4,23,17{
	offset 10,16
	patch RIFBC,-134,0{}
}
sprite RIGLB7B3,42,17{
	offset 20,16
	patch RIFBC,-157,0{}
}
sprite RIGLB8B2,23,17{
	offset 10,16
	patch RIFBC,-199,0{}
}
sprite RIGLB1,7,17{
	offset 4,16
	patch RIFBC,-222,0{}
}
sprite RIFLC5,9,17{
	offset 5,16
	patch RIFBC,0,-17{}
}
sprite RIFLC6C4,29,17{
	offset 10,16
	patch RIFBC,-9,-17{}
}
sprite RIFLC7C3,51,17{
	offset 20,16
	patch RIFBC,-38,-17{}
}
sprite RIFLC8C2,29,17{
	offset 10,16
	patch RIFBC,-89,-17{}
}
sprite RIFLC1,9,17{
	offset 5,16
	patch RIFBC,-118,-17{}
}
sprite RIGLC5,9,17{
	offset 5,16
	patch RIFBC,0,0{}
}
sprite RIGLC6C4,29,17{
	offset 10,16
	patch RIFBC,-9,0{}
}
sprite RIGLC7C3,51,17{
	offset 20,16
	patch RIFBC,-38,0{}
}
sprite RIGLC8C2,29,17{
	offset 10,16
	patch RIFBC,-89,0{}
}
sprite RIGLC1,9,17{
	offset 5,16
	patch RIFBC,-118,0{}
}
sprite RCLSA1A5,1,1{offset 1,1 patch RBRSA1A5,0,0{translation "0:255=%[0,0,0]:[0.6,0.4,0.1]"}} //=ZMRound in TRNSLATE
sprite RCLSA4A6,2,1{offset 1,1 patch RBRSA4A6,0,0{translation "0:255=%[0,0,0]:[0.6,0.4,0.1]"}}
sprite RCLSA2A8,2,1{offset 1,1 patch RCLSA4A6,0,0{flipx}}
sprite RCLSA3A7,5,1{offset 3,1 patch RBRSA3A7,0,0{translation "0:255=%[0,0,0]:[0.6,0.4,0.1]"}}
sprite RCLSB0,5,1{
	offset 3,2
	patch RBRSA3A7,0,0{translation "0:255=%[0,0,0]:[0.6,0.4,0.1]"}
	patch RBRSA3A7,0,1{translation "0:255=%[0,0,0]:[0.6,0.4,0.1]"}
	patch RBRSA3A7,0,2{translation "0:255=%[0,0,0]:[0.6,0.4,0.1]"}
}



//Liberator pickup
sprite BRFLG0,42,17{
	offset 19,16
	patch LIBBASE,0,0{}
}
sprite BRFLH0,42,17{
	offset 19,16
	patch BRFLG0,0,0{}
	patch RIFGLAT,20,10{}
	patch LADSITE,28,4{}
}
sprite BRFLE0,42,20{
	offset 19,19
	patch LIBMAGAT,4,12{}
	patch BRFLG0,0,0{}
}
sprite BRFLF0,42,20{
	offset 19,19
	patch LIBMAGAT,4,12{}
	patch BRFLH0,0,0{}
}
//Liberator pickup with fire selector
sprite BRFLC0,42,17{
	offset 19,16
	patch BRFLG0,0,0{}
	patch FIRESEL,17,8{}
}
sprite BRFLD0,42,17{
	offset 19,16
	patch BRFLH0,0,0{}
	patch FIRESEL,17,8{}
}
sprite BRFLA0,42,20{
	offset 19,19
	patch BRFLE0,0,0{}
	patch FIRESEL,17,8{}
}
sprite BRFLB0,42,20{
	offset 19,19
	patch BRFLF0,0,0{}
	patch FIRESEL,17,8{}
}

//Liberator pickup - no bullpup
sprite BRLLG0,57,14{
	offset 24,14
	patch LIBBASE,0,-17{}
}
sprite BRLLH0,57,16{
	offset 24,14
	patch BRLLG0,0,0{}
	patch RIFGLAT,26,9{}
	patch LADSITE,33,5{}
}
sprite BRLLE0,57,20{
	offset 24,20
	patch LIBMAGAT,20,11{}
	patch BRLLG0,0,0{}
}
sprite BRLLF0,57,20{
	offset 24,20
	patch LIBMAGAT,20,11{}
	patch BRLLH0,0,0{}
}
//Liberator pickup with fire selector - no bullpup
sprite BRLLC0,57,14{
	offset 24,14
	patch BRLLG0,0,0{}
	patch FIRESEL,20,9{}
}
sprite BRLLD0,57,16{
	offset 24,14
	patch BRLLH0,0,0{}
	patch FIRESEL,20,9{}
}
sprite BRLLA0,57,20{
	offset 24,20
	patch BRLLE0,0,0{}
	patch FIRESEL,20,9{}
}
sprite BRLLB0,57,20{
	offset 24,20
	patch BRLLF0,0,0{}
	patch FIRESEL,20,9{}
}


sprite BSHXA0,6,8{
	offset 3,7
	patch BSHXC8C6,0,0{rotate 90}
}
sprite BSHXB0,6,8{
	offset 3,7
	patch BSHXC2C4,0,0{rotate 270}
}


//Shotgun shell
sprite SHL1A0,3,8{
	offset 1,8
	patch ESHLC0,0,0{translation "176:191=152:159","32:47=106:111"}
}
sprite SHELA0,12,8{
	offset 6,8
	patch SHL1A0,0,0{}
	patch SHL1A0,3,0{}
	patch SHL1A0,6,0{}
	patch SHL1A0,9,0{}
}



//Shotgun pickups
graphic SMSHBRT,1,3{
	patch PRNDA0,0,0{flipy translation "160:167=152:154"}
}
graphic SMSHDRK,1,3{
	patch SMSHBRT,0,0{blend "black",0.3}
}
sprite HUNTG0,50,12{
	offset 25,12
	patch SHOTGUNS,0,-11{}
}
sprite HUNTF0,50,12{
	offset 25,12
	patch HUNTG0,0,0{}
	patch SMSHBRT,2,4{}
}
sprite HUNTE0,50,12{
	offset 25,12
	patch HUNTF0,0,0{}
	patch SMSHDRK,3,4{}
}
sprite HUNTD0,50,12{
	offset 25,12
	patch HUNTE0,0,0{}
	patch SMSHBRT,4,4{}
}
sprite HUNTC0,50,12{
	offset 25,12
	patch HUNTD0,0,0{}
	patch SMSHBRT,6,4{}
}
sprite HUNTB0,50,12{
	offset 25,12
	patch HUNTC0,0,0{}
	patch SMSHDRK,7,4{}
}
sprite HUNTA0,50,12{
	offset 25,12
	patch HUNTB0,0,0{}
	patch SMSHBRT,8,4{}
}
sprite SLAYG0,52,10{
	offset 26,12
	patch SHOTGUNS,0,0{}
}
sprite SLAYF0,52,10{
	offset 26,12
	patch SLAYG0,0,0{}
	patch SMSHBRT,4,5{}
}
sprite SLAYE0,54,10{
	offset 26,12
	patch SLAYF0,0,0{}
	patch SMSHDRK,5,5{}
}
sprite SLAYD0,52,10{
	offset 26,12
	patch SLAYE0,0,0{}
	patch SMSHBRT,7,6{}
}
sprite SLAYC0,52,10{
	offset 26,12
	patch SLAYD0,0,0{}
	patch SMSHDRK,8,6{}
}
sprite SLAYB0,52,10{
	offset 26,12
	patch SLAYC0,0,0{}
	patch SMSHBRT,10,6{}
}
sprite SLAYA0,52,10{
	offset 26,12
	patch SLAYB0,0,0{}
	patch SMSHBRT,11,5{}
}




//Vulcanette
sprite GTLGA0,117,72{
	offset -119,-124
	xscale 1.1
	yscale 1.1
	patch VUBGA0,0,0{}
}
sprite GTLGB0,117,72{
	offset -119,-124
	xscale 1.1
	yscale 1.1
	patch VUBGB0,0,0{}
}
sprite VULFA0,62,49{
	offset -144,-88
	xscale 1.1
	yscale 1.1
	patch VUBFA0,0,0{}
}
sprite VULFB0,50,32{
	offset -147,-104
	xscale 1.1
	yscale 1.1
	patch VUBFB0,0,0{}
}





/*
    Weapon HUD
*/

graphic HDXHCAM1BLANK,64,56{
	patch FLAT19,0,0{translation "0:255=%[0,0,0]:[0.24,0.2,0.2]"}
	patch FLAT19,64,0{translation "0:255=%[0,0,0]:[0.24,0.2,0.2]"}
	patch FLAT19,0,64{translation "0:255=%[0,0,0]:[0.24,0.2,0.2]"}
	patch FLAT19,64,64{translation "0:255=%[0,0,0]:[0.24,0.2,0.2]"}
}
graphic HDPISAMO,6,6
{
	patch PBRSC0G0,0,0 {}
	patch PBRSC0G0,0,2 {}
	patch PBRSC0G0,0,4 {}
	patch PBRSC0G0,3,0 {}
	patch PBRSC0G0,3,2 {}
	patch PBRSC0G0,3,4 {}
}
graphic MAGSTAT,11,8
{
	offset 4,8
	patch RBRSA3A7,7,0{flipx}
	patch RBRSA3A7,7,2{flipx}
	patch RBRSA3A7,7,4{flipx}
	patch RBRSA3A7,7,6{flipx}
}
graphic MAGRED,11,8
{
	offset 4,8
	patch RBRSA3A7,7,0{translation red,flipx}
	patch RBRSA3A7,7,2{translation red,flipx}
	patch RBRSA3A7,7,4{translation red,flipx}
	patch RBRSA3A7,7,6{translation red,flipx}
}
graphic CLIPRED,9,11
{
	offset 2,11
	patch CMAGA0,0,0{translation red}
}
graphic CLIPGREY,9,11
{
	offset 2,11
	patch CMAGA0,0,0{translation "0:255=%[0,0,0]:[1.8,1.8,1.8]"}
}
sprite ZMAGA0,9,11{
	offset 5,11
	xscale 2.7
	yscale 2.0
	patch CMAGA0,0,0{translation "160:167=176:183"}
}
sprite ZMAGB0,9,11{
	offset 5,11
	xscale 2.7
	yscale 2.0
	patch CMAGA0,0,0{translation "160:167=64:79","224:231=48:63"}
}
graphic ZMAGNORM,9,11{
	offset 5,11
	xscale 1.35
	yscale 1.0
	patch ZMAGA0,0,0{}
}
graphic ZMAGBROWN,9,11{
	offset 5,11
	xscale 1.35
	yscale 1.0
	patch CMAGA0,0,0{translation "160:167=64:79","224:231=48:63"}
}
graphic ZMAGGREY,9,11{
	offset 5,11
	xscale 1.35
	yscale 1.0
	patch ZMAGB0,0,0{translation "0:255=%[0,0,0]:[1.8,1.8,1.8]"}
}
sprite ZMAGC0,11,9{
	offset 6,9
	xscale 1.6
	yscale 3.2
	patch CMAGA0,0,0{translation "160:167=80:87";rotate 270}
}
sprite CLP2A0,9,11
{
	offset 2,11
	xscale 4.3
	yscale 2.7
	patch CMAGA0,0,0{translation "97:111=240:247"}
}
graphic CLP2NORM,9,11
{
	offset 2,11
	xscale 1.433
	yscale 0.9
	patch CLP2A0,0,0{}
}
graphic CLP2EMPTY,11,9{
	offset 5,9
	xscale 0.75
	yscale 1.72
	patch CMAGA0,0,0{translation "97:111=240:247","161:167=104:111"; rotate 270}
}
graphic CLP2GREY,9,11
{
	offset 2,11
	xscale 1.433
	yscale 0.9
	patch CMAGA0,0,0{translation "0:255=%[0,0,0]:[1.8,1.8,1.8]"}
}
texture CLP3X0,9,3
{
	patch CMAGA0,0,0{translation "97:111=106:111"}
}
texture CLP3Y0,9,5
{
	yscale 0.5
	patch CMAGA0,0,-3{translation "97:111=106:111"}
}
sprite CLP3A0,9,20
{
	offset 2,19
	xscale 4.3
	yscale 2.7
	patch CMAGA0,0,10{translation "97:111=106:111"}
	patch CLP3Y0,0,8{}
	patch CLP3Y0,0,4{}
	patch CLP3Y0,0,2{}
	patch CLP3X0,0,0{}
}
graphic CLP3NORM,9,20
{
	offset 2,19
	xscale 1.433
	yscale 0.9
	patch CLP3A0,0,0{}
}
graphic CLP3GREY,9,20
{
	offset 2,19
	xscale 1.433
	yscale 0.9
	patch CLP3A0,0,0{translation "0:255=%[0,0,0]:[1.8,1.8,1.8]"}
}
sprite CLP2B0,11,9{
	offset 5,9
	xscale 2.25
	yscale 5.16
	patch CMAGA0,0,0{translation "97:111=240:247","161:167=104:111"; rotate 270}
}
sprite CLP3B0,20,9{
	offset 10,9
	xscale 2.2
	yscale 4.5
	patch CMAGA0,10,0{translation "97:111=106:111";rotate 270}
	patch CLP3Y0,8,0{rotate 270}
	patch CLP3Y0,4,0{rotate 270}
	patch CLP3Y0,1,0{rotate 270}
	patch CLP3X0,0,0{translation "161:167=104:111";rotate 270}
}
sprite CELLA0,17,12{
	offset 8,12
	patch BATTA0,0,0{}
}
sprite CELLB0,17,12{
	offset 8,12
	patch CELLA0,0,0{translation "112:120=160:167"}
}
sprite CELLC0,17,12{
	offset 8,12
	patch CELLA0,0,0{translation "112:120=32:47"}
}
sprite CELLD0,17,12{
	offset 8,12
	patch CELLA0,0,0{translation "112:120=96:111"}
}
graphic PISARED,6,6
{
	patch PBRSC0G0,0,0 {translation red}
	patch PBRSC0G0,0,2 {translation red}
	patch PBRSC0G0,0,4 {translation red}
	patch PBRSC0G0,3,0 {translation red}
	patch PBRSC0G0,3,2 {translation red}
	patch PBRSC0G0,3,4 {translation red}
}
graphic ARMER0,37,28{
	offset 18,27
	patch ARMSA0,0,0{translation "0:255=%[0,0,0]:[0.3,0.14,0]"}
}
graphic ARMER1,37,26{
	offset 18,26
	patch ARMCA0,0,0{translation "0:255=%[0,0,0]:[0.3,0.14,0]"}
}
graphic STBURAUT,5,3
{
	patch PBRSC0G0,0,2 {}
	patch PBRSC0G0,1,0 {}
	patch PBRSC0G0,2,2 {}
}
graphic STFULAUT,15,3
{
	patch PBRSC0G0,0,0 {}
	patch PBRSC0G0,2,2 {}
	patch PBRSC0G0,4,0 {}
	patch PBRSC0G0,6,2 {}
	patch PBRSC0G0,8,0 {}
	patch PBRSC0G0,10,2 {}
	patch PBRSC0G0,12,0 {}
}
sprite PISTA0,27,15
{
	offset 13,15
	xscale 1.5
	yscale 1.5
	patch PISTA0,0,0{}
}
sprite PISTY0,27,12
{
	patch PISTA0,0,-3{}
}
sprite PISTZ0,27,5
{
	patch PISTA0,0,0{}
}
sprite PISTB0,30,15{
	offset 16,15
	xscale 1.5
	yscale 1.5
	patch PISTY0,3,3{}
	patch PISTZ0,0,0{}
}
sprite PISTC0,27,15{
	offset 13,15
	xscale 1.5
	yscale 1.5
	patch PISTA0,0,0{}
	patch FIRESEL,11,5{}
}
sprite PISTD0,30,15{
	offset 16,15
	xscale 1.5
	yscale 1.5
	patch PISTB0,0,0{}
	patch FIRESEL,14,5{}
}
sprite ROCKB0,12,27{
	offset 6,26
	xscale 1.8
	yscale 1.8
	patch ROCKA0,0,0{}
}



// H.E.R.P.
sprite HERPA1,9,15{
	offset 5,15
	patch HERPBOT,0,0{}
}
sprite HERPB1,9,15{
	offset 5,15
	xscale 1.05
	yscale 1.05
	patch HERPBOT,0,-61{}
}
sprite HERPC1,9,15{
	offset 5,15
	patch HERPBOT,0,-15{}
}

sprite HERPA5,9,15{
	offset 5,15
	patch HERPBOT,-36,0{}
}
sprite HERPB5,9,15{
	offset 5,15
	xscale 0.95
	yscale 0.95
	patch HERPBOT,-36,-15{}
}
sprite HERPC5,9,15{
	offset 5,15
	patch HERPBOT,-36,0{}
}

sprite HERPA8A2,19,15{
	offset 8,15
	patch HERPBOT,0,-31{}
}
sprite HERPB8B2,25,15{
	offset 8,15
	patch HERPBOT,0,-76{}
}
sprite HERPC8C2,19,15{
	offset 8,15
	xscale 0.98
	yscale 0.98
	patch HERPBOT,-19,-31{}
}

sprite HERPA6A4,19,15{
	offset 8,15
	patch HERPBOT,0,-46{}
}
sprite HERPB6B4,24,15{
	offset 8,15
	patch HERPBOT,-25,-76{}
}
sprite HERPC6C4,19,15{
	offset 8,15
	xscale 1.02
	yscale 1.02
	patch HERPBOT,-19,-46{}
}

sprite HERPA7A3,27,15{
	offset 12,15
	patch HERPBOT,-9,0{}
}
sprite HERPB7B3,36,15{
	offset 13,15
	patch HERPBOT,-9,-61{}
}
sprite HERPC7C3,27,15{
	offset 12,15
	patch HERPBOT,-9,-15{}
}

sprite HLEGA0,6,14{
	offset 3,14
	patch HERPBOT,-38,-31{}
}
graphic HERPEX,25,15{
	offset -8,-10
	patch HERPB8B2,0,0{}
}


//H.E.R.P. (manual held fire)
sprite HERGA0,114,81{
	offset -169,-190
	xscale 1.4
	yscale 1.4
	patch CHGGA0,0,0{}
}
sprite HERGB0,114,81{
	offset -169, -197
	xscale 1.4
	yscale 1.4
	patch CHGGB0,0,0{}
}
sprite HERFA0,86,46{
	offset -188,-170
	xscale 1.4
	yscale 1.4
	patch CHGFA0,0,0{}
}




// D.E.R.P.
graphic DERPEX,20,14{
	offset -8,-10
	patch DERPBOT,-67,-17{}
	patch PBRSB0H0,13,0{}
	patch PBRSA0,19,3{}
}

graphic DPICA5, 13,14{
	Offset 7,13
	Patch DERPBOT,0,0{}
}
sprite DERPA1,13,14{
	offset 7,13
	patch DERPBOT,-89,0{}
}
sprite DERPA5,13,14{
	offset 7,13
	patch DERPBOT,0,0{}
}
sprite DERPA8A2,15,14{
	offset 6,13
	patch DERPBOT,-67,0{}
}
sprite DERPA7A3,20,14{
	offset 9,13
	patch DERPBOT,-37,0{}
}
sprite DERPA6A4,16,14{
	offset 6,13
	patch DERPBOT,-16,0{}
}

sprite DERPC1,13,14{
	offset 7,13
	patch DERPBOT,-89,-17{}
}
sprite DERPC5,13,14{
	offset 7,13
	patch DERPBOT,0,-17{}
}
sprite DERPC8C2,20,14{
	offset 6,13
	patch DERPBOT,-67,-17{}
}
sprite DERPC7C3,29,14{
	offset 9,13
	patch DERPBOT,-37,-17{}
}
sprite DERPC6C4,19,14{
	offset 6,13
	patch DERPBOT,-16,-17{}
}

sprite DERPD1,13,14{
	offset 7,13
	patch DERPBOT,-89,-33{}
}
sprite DERPD5,13,14{
	offset 7,13
	patch DERPBOT,0,-33{}
}
sprite DERPD8D2,15,13{
	offset 6,13
	patch DERPBOT,-67,-33{}
}
sprite DERPD7D3,20,14{
	offset 9,13
	patch DERPBOT,-37,-33{}
}
sprite DERPD6D4,16,13{
	offset 6,13
	patch DERPBOT,-16,-33{}
}




/*
     Extra smoke effects
*/

sprite BAL1F0,37,35{
	offset 19,18
	patch BAL1C0,0,0{translation desaturate,31}
}
sprite BAL1G0,43,39{
	offset 22,22
	patch BAL1D0,0,0{translation desaturate,31}
}
sprite BAL1H0,50,44{
	offset 25,24
	patch BAL1E0,0,0{translation desaturate,31}
}



//IEDs
sprite IEDSB0,15,24{
	offset 8,23
	patch IEDSA0,0,0{translation "112:127=176:191"}
}
sprite IEDSC0,15,24{
	offset 8,23
	patch IEDSA0,0,0{translation "112:127=96:111"}
}


//radsuit
graphic SUITB0,24,22{
	offset -5,-5
	patch SUITA0,0,0{}
}
graphic SUITC0,20,15{
	patch SUITA0,-3,-37{}
}


sprite RMAGB0,20,13{
	offset 10,7
	xscale 1.6
	yscale 2.1
	patch RMAGA0,0,0{rotate 270,translation "160:167=104:111"}
}
sprite RMAGA0,13,20{
	offset 7,10
	xscale 1.9
	yscale 1.9
	patch RMAGA0,0,0{}
}
graphic RMAGNORM,13,20{
	offset 7,10
	xscale 1.3
	yscale 1.3
	patch RMAGA0,0,0{}
}
graphic RMAGGREY,13,20{
	offset 7,10
	xscale 1.3
	yscale 1.3
	patch RMAGA0,0,0{translation "0:255=%[0,0,0]:[1.8,1.8,1.8]"}
}
graphic RMAGEMPTY,20,13{
	offset 10,7
	xscale 1.0833
	yscale 1.56
	patch RMAGA0,0,0{rotate 270,translation "160:167=104:111"}
}
sprite CLIPB0,11,9{
	offset 5,9
	patch CMAGA0,0,0{rotate 90}
}
sprite CLIPC0,9,11{
	offset 2,11
	patch CMAGA0,0,0 {translation "160:167=176:191"}
}
graphic RBRSRED,4,1{
	patch RBRSA3A7,0,0{translation "red"}
}
graphic RBRSBRN,4,1{
	patch RBRSA3A7,0,0{translation "0:255=%[0,0,0]:[0.6,0.4,0.1]"}
}


//single pistol round
sprite PRNDA0,1,3{
	offset 1,3
	patch TEN9A0,0,-1{}
}



#include "textures.gun"

#include "textures.sights"

